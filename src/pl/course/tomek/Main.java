package pl.course.tomek;

public class Main {

    public static void main(String[] args) {
	Rectangle rectangle = new Rectangle(2.5,5);
        System.out.println("rectangle.width= "+rectangle.getWidth());
        System.out.println("rectangle.length= "+rectangle.getLength());
        System.out.println("rectangle.area= "+rectangle.getArea());

        Cuboid cuboid = new Cuboid(1,3,5);
        System.out.println("cuboid.width= "+cuboid.getWidth());
        System.out.println("cuboid.length= "+cuboid.getLength());
        System.out.println("cuboid.height= "+cuboid.getHeight());
        System.out.println("cuboid.area= "+cuboid.getArea());
        System.out.println("cuboid.volume= "+cuboid.getVolume());

        Rectangle badRectangle = new Rectangle(3,-3);
        System.out.println("badRectangle.width= "+badRectangle.getWidth());
        System.out.println("badRectangle.length= "+badRectangle.getLength());
        System.out.println("badRectangle.area= "+badRectangle.getArea());

        Cuboid badCuboid = new Cuboid(1,3,-5);
        System.out.println("badCuboid.width= "+badCuboid.getWidth());
        System.out.println("badCuboid.length= "+badCuboid.getLength());
        System.out.println("badCuboid.height= "+badCuboid.getHeight());
        System.out.println("badCuboid.area= "+badCuboid.getArea());
        System.out.println("badCuboid.volume= "+badCuboid.getVolume());
    }
}
